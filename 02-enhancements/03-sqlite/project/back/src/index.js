import app from './app'
import db from './db'

db.test()

app.get( '/', (req, res) => res.send("ok") );

app.listen( 8080, () => console.log('server listening on port 8080') )